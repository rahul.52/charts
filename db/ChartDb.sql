USE [ChartDb]
GO
/****** Object:  Table [dbo].[chartString]    Script Date: 16/06/20 10:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chartString](
	[QueryString] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChartType]    Script Date: 16/06/20 10:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChartType](
	[Sn] [int] NULL,
	[ChartName] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Data]    Script Date: 16/06/20 10:59:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Data](
	[Month] [nchar](10) NULL,
	[Count] [nchar](10) NULL,
	[Count2] [nchar](10) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[chartString] ([QueryString]) VALUES (N'select Month as X , Count as Y1 , Count2 as Y2  from Data')
GO
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (1, N'bar')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (2, N'horizontalBar')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (3, N'pie')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (4, N'line')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (5, N'doughnut')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (6, N'radar')
INSERT [dbo].[ChartType] ([Sn], [ChartName]) VALUES (7, N'polarArea')
GO
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Jan       ', N'20000     ', N'16236     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Feb       ', N'17630     ', N'20145     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Mar       ', N'11256     ', N'13269     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Apr       ', N'22362     ', N'10859     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'May       ', N'9025      ', N'19585     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Jun       ', N'16360     ', N'11275     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Jul       ', N'31005     ', N'26513     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Aug       ', N'14500     ', N'19854     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Sep       ', N'27456     ', N'35269     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Oct       ', N'8546      ', N'13258     ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Nov       ', N'13365     ', N'9632      ')
INSERT [dbo].[Data] ([Month], [Count], [Count2]) VALUES (N'Dec       ', N'28036     ', N'21458     ')
GO
