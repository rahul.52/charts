﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Charts.Models
{
    public class ChartIndices
    {
        public string X { get; set; }
        public int Y1 { get; set; }
        public int Y2 { get; set; }
        public List<string> Xaxis { get; set; }
        public List<int> Y1axis { get; set; }
        public List<int> Y2axis { get; set; }
        public List<string> color1 { get; set; }
        public List<string> color2 { get; set; }
    }
}