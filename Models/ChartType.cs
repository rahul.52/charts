﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Charts.Models
{
    public class ChartType
    {
        public int Sn { get; set; }
        public string ChartName { get; set; }
    }
}