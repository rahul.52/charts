﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Charts.Models;
using Dapper;

namespace Charts.Controllers
{
    public class ChartController : Controller
    {
        // GET: Chart
        readonly IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        public ActionResult Index()
        {
            List<ChartType> ctList = new List<ChartType>();
            ctList = db.Query<ChartType>("select * from ChartType").ToList();
            ViewBag.ChartName = new SelectList(ctList, "ChartName", "ChartName");
            return View();
        }

        public JsonResult chat()
        {
            string qry = db.Query<chartString>("select QueryString from chartString").SingleOrDefault().QueryString;

            List<ChartIndices> dataList = new List<ChartIndices>();
            dataList = db.Query<ChartIndices>(qry).ToList();

            db.Close();
            ChartIndices data = new ChartIndices();
            data.Y1axis = new List<int>();
            data.Y2axis = new List<int>();
            data.Xaxis = new List<string>();
            data.color1 = new List<string>();
            data.color2 = new List<string>();
            var random = new Random();
            foreach (var item in dataList)
            {
                var Datacolor1 = String.Format("#{0:X6}", random.Next(0x1000000));
                var Datacolor2 = String.Format("#{0:X6}", random.Next(0x1000000));
                data.Xaxis.Add(item.X);
                data.Y1axis.Add(item.Y1);
                data.Y2axis.Add(item.Y2);
                data.color1.Add(Datacolor1);
                data.color2.Add(Datacolor2);
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}

